import logging

from django.urls import include, re_path

from NEMO_reporting import reports

logger = logging.getLogger(__name__)

urlpatterns = [
    # Reports:
    re_path(r'^reports/$', reports.reports, name='reports'),
    re_path(r'^reports/usage-events/$', reports.usage_events, name='usage_events'),
    re_path(r'^reports/active-users/$', reports.active_users, name='active_users'),
    re_path(r'^reports/cumulative-users/$', reports.cumulative_users, name='cumulative_users'),
    re_path(r'^reports/groups/$', reports.groups, name='groups'),
    re_path(r'^reports/facility-usage/$', reports.facility_usage, name='facility_usage'),
    re_path(r'^reports/invoice/$', reports.invoice, name='invoice'),
    re_path(r'^reports/aging-schedule/$', reports.aging_schedule, name='aging_schedule'),
    re_path(r'^reports/area-events/$', reports.area_events, name='area_events')
]
