import collections
import itertools
import pandas as pd
from NEMO.decorators import facility_manager_required

from dateutil import parser, tz
from NEMO.models import Account, UsageEvent, User, AreaAccessRecord
from datetime import datetime
from django.apps import apps
from django.shortcuts import render
from django.utils import timezone


@facility_manager_required
def reports(request):
    billing_invoices = apps.is_installed("NEMO_billing")
    return render(request, "reports/reports.html", {"billing_invoices": billing_invoices})


def convert_timedelta(duration):
    days, seconds = duration.days, duration.seconds
    hours = days * 24 + seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = (seconds % 60)
    return '{}:{:02d}:{:02d}'.format(hours, minutes, seconds)


def parse_start_end_date(start, end):
    start = timezone.make_aware(parser.parse(start), timezone.get_current_timezone())
    end = timezone.make_aware(parser.parse(end), timezone.get_current_timezone())
    return start, end


def date_parameters_dictionary(request):
    data = dict(request.POST)
    if data.get('start_time') and data.get('end_time'):
        start_date, end_date = parse_start_end_date(data.get('start_time')[0], data.get('end_time')[0])
    else:
        start_date, end_date = '0', '0'
    return start_date, end_date


@facility_manager_required
def usage_events(request):
    start_date, end_date = date_parameters_dictionary(request)
    if start_date != '0' or end_date != '0':
        tool_data = UsageEvent.objects.only("tool", "start", "end").select_related('tool').filter(end__gt=start_date,
                                                                                                  end__lte=end_date).order_by(
            'tool')
        d = {}
        print(start_date)
        print(end_date)
        for tool in tool_data:
            start = tool.start
            if tool.end:
                end = tool.end
                if tool.tool not in d:
                    d[tool.tool] = end - start
                else:
                    d[tool.tool] += end - start
        keys_values = d.items()
        new_d = {str(key): str(convert_timedelta(value)) for key, value in keys_values}
        print(new_d)
        return render(request, "reports/usage_events.html", {'context': new_d, 'start': start_date, 'end': end_date})
    else:
        return render(request, "reports/usage_events.html", {'start': start_date, 'end': end_date})


@facility_manager_required
def area_events(request):
    start_date, end_date = date_parameters_dictionary(request)
    if start_date != '0' or end_date != '0':
        area_data = AreaAccessRecord.objects.only("area", "start", "end").select_related('area').filter(
            end__gt=start_date,
            end__lte=end_date)
        d = {}
        print(start_date)
        print(end_date)
        for area in area_data:
            start = area.start
            if area.end:
                end = area.end
                if area.area not in d:
                    d[area.area] = end - start
                else:
                    d[area.area] += end - start
        keys_values = d.items()
        new_d = {str(key): str(convert_timedelta(value)) for key, value in keys_values}
        print(new_d)
        return render(request, "reports/area_events.html", {'context': new_d, 'start': start_date, 'end': end_date})
    else:
        return render(request, "reports/area_events.html", {'start': start_date, 'end': end_date})


@facility_manager_required
def active_users(request):
    start_date, end_date = date_parameters_dictionary(request)
    list_of_data = [[] for i in range(3)]
    if start_date != '0' or end_date != '0':
        tool_data = UsageEvent.objects.only("user", "end").select_related('user').filter(end__gt=start_date,
                                                                                         end__lte=end_date).order_by(
            "user")
        d = {}
        for tool in tool_data:
            d[tool.user] = tool.user
            if tool.user.username not in list_of_data[2]:
                list_of_data[0].append(tool.user.first_name)
                list_of_data[1].append(tool.user.last_name)
                list_of_data[2].append(tool.user.username)
        print(list_of_data)
        keys_values = d.items()
        new_d = {str(key): str(value) for key, value in keys_values}
        list_output = list(map(list, itertools.zip_longest(*list_of_data, fillvalue=None)))
        print(list_output)
        if len(new_d) != 0:
            total = len(new_d)
            return render(request, "reports/active_users.html", {'context': list_output, 'total': total,
                                                                 'start': start_date, 'end': end_date})
        else:
            return render(request, "reports/active_users.html", {'start': start_date, 'end': end_date})
    else:
        return render(request, "reports/active_users.html", {'start': start_date, 'end': end_date})


@facility_manager_required
def cumulative_users(request):
    start_date, end_date = date_parameters_dictionary(request)
    list_of_data = [[] for i in range(5)]
    if start_date != '0' or end_date != '0':
        user_data = User.objects.only("first_name", "last_name", "type", "date_joined", "username").filter(
            date_joined__gte=start_date, date_joined__lte=end_date).order_by("date_joined")
        print(user_data)
        for user in user_data:
            list_of_data[0].append(user.first_name)
            list_of_data[1].append(user.last_name)
            list_of_data[2].append(user.username)
            list_of_data[3].append(user.type)
            list_of_data[4].append(str(user.date_joined)[0:10])
        list_output = list(map(list, itertools.zip_longest(*list_of_data, fillvalue=None)))
        return render(request, "reports/cumulative_users.html",
                      {'context': list_output, 'start': start_date, 'end': end_date})
    else:
        return render(request, "reports/cumulative_users.html", {'start': start_date, 'end': end_date})


@facility_manager_required
def groups(request):
    start_date, end_date = date_parameters_dictionary(request)
    list_of_data = [[] for i in range(3)]
    if start_date != '0' or end_date != '0':
        groups_data = Account.objects.filter(start_date__gte=start_date, start_date__lte=end_date).order_by(
            'start_date')
        for group in groups_data:
            list_of_data[0].append(group.name)
            list_of_data[1].append(group.type)
            list_of_data[2].append(str(group.start_date))
        print(list_of_data[1])
        breakdown = collections.Counter(list_of_data[1]).most_common()
        print(type(breakdown))
        print(breakdown)
        list_output = list(map(list, itertools.zip_longest(*list_of_data, fillvalue=None)))
        return render(request, "reports/groups.html", {'context': list_output, 'breakdown': breakdown,
                                                       'start': start_date, 'end': end_date})
    else:
        return render(request, "reports/groups.html", {'start': start_date, 'end': end_date})


@facility_manager_required
def facility_usage(request):
    from NEMO_billing.invoices.models import ProjectBillingDetails
    start_date, end_date = date_parameters_dictionary(request)
    if start_date != '0' or end_date != '0':
        facility_data = UsageEvent.objects.only("project", "start", "end").select_related('project').filter(
            end__gt=start_date, end__lte=end_date)
        project_list = UsageEvent.objects.only("project", "start", "end").select_related('project'). \
            filter(end__gt=start_date, end__lte=end_date).values_list('project')
        project_data = ProjectBillingDetails.objects.only("project", "category", "no_charge").select_related(
            'category').filter(
            project__in=project_list)
        d = {}
        d_category = {}
        category = []
        for project in project_data:
            d_category[project] = project.category
            category.append(project.category)

        for facility in facility_data:
            start = facility.start
            if facility.end:
                end = facility.end
                if facility.project not in d:
                    d[facility.project] = end - start
                else:
                    d[facility.project] += end - start
        keys_values = d.items()
        new_d = {str(key): str(convert_timedelta(value)) for key, value in keys_values}
        datetime_d = {str(key): value for key, value in keys_values}
        df1 = pd.DataFrame(new_d.items(), columns=['project', 'time'])
        df1['project'] = df1['project'].astype(str)
        df2 = pd.DataFrame(d_category.items(), columns=['project', 'category'])
        df2['project'] = df2['project'].astype(str)
        df3 = pd.DataFrame(datetime_d.items(), columns=['project', 'time'])
        df3['project'] = df3['project'].astype(str)
        left_join = pd.merge(df1, df2, on='project', how='left')
        datetime_left_join = pd.merge(df3, df2, on='project', how='left')
        by_category = datetime_left_join.drop('project', axis=1)
        by_category = by_category.astype({"category": str})
        group_category = by_category.groupby('category')['time'].sum()
        category_output = group_category.reset_index()
        category_output['time'] = category_output['time'].apply(lambda x: str(convert_timedelta(x)))
        category_output['category'] = category_output['category'].replace('nan', 'N/A')
        all_join = left_join.to_dict('records')
        category_dict = category_output.to_dict('records')
        return render(request, "reports/facility_usage.html", {'context': all_join, 'category': category_dict,
                                                               'start': start_date, 'end': end_date})
    else:
        return render(request, "reports/facility_usage.html", {'start': start_date, 'end': end_date})


@facility_manager_required
def invoice(request):
    from NEMO_billing.invoices.models import Invoice
    from NEMO_billing.invoices.models import InvoiceSummaryItem
    start_date, end_date = date_parameters_dictionary(request)
    list_of_data = [[] for i in range(3)]
    if start_date != '0' or end_date != '0':
        invoice_data = Invoice.objects.only("id", "start", "project_details", "voided_date").filter(
            created_date__gt=start_date, created_date__lte=end_date).order_by("start")
        invoice_list = Invoice.objects.only("total_amount", "start").filter(
            created_date__gt=start_date, created_date__lte=end_date).values_list('id')
        invoicesummary_data = InvoiceSummaryItem.objects.only("invoice", "amount", "core_facility", "name")\
            .select_related('invoice').filter(invoice_id__in=invoice_list).filter(name="Subtotal")

        for each in invoice_data:
            if each.voided_date is None:
                list_of_data[0].append(str(str(each.start.year) + "-" + each.start.strftime("%m")))
                list_of_data[1].append(int(each.id))
                list_of_data[2].append(str(each.project_details.category))
        list_transpose = list(map(list, itertools.zip_longest(*list_of_data, fillvalue=None)))
        df1 = pd.DataFrame(list_transpose, columns=['Period', 'Invoice', 'Project'])

        list_of_summarydata = [[] for i in range(3)]
        for invoicesummary in invoicesummary_data:
            list_of_summarydata[0].append(invoicesummary.core_facility)
            list_of_summarydata[1].append(int(invoicesummary.invoice_id))
            list_of_summarydata[2].append(invoicesummary.amount)
        list_summary_transpose = list(map(list, itertools.zip_longest(*list_of_summarydata, fillvalue=None)))
        df2 = pd.DataFrame(list_summary_transpose, columns=['Facility', 'Invoice', 'Amount'])
        left_join = pd.merge(df1, df2, on='Invoice', how='left')
        left_join = left_join.drop('Invoice', axis=1)
        row_sum = left_join.groupby(['Period', 'Project', 'Facility']).agg('sum')
        result = row_sum.reset_index()
        if not result.empty:
            result['Amount'] = result['Amount'].apply(lambda x: "${:,.2f}".format(x))
        joined = result.to_dict('records')
        return render(request, "reports/invoices.html",
                      {'context': joined, 'start': start_date, 'end': end_date})
    else:
        return render(request, "reports/invoices.html", {'start': start_date, 'end': end_date})


def utc_to_local(utc_now):
    # Get local timezone
    local_zone = tz.tzlocal()
    # Convert timezone of datetime from UTC to local
    local_now = utc_now.astimezone(local_zone)
    return local_now


@facility_manager_required
def aging_schedule(request):
    from NEMO_billing.invoices.models import Invoice
    from NEMO_billing.invoices.models import InvoicePayment
    start_date, end_date = date_parameters_dictionary(request)
    list_of_data = [[] for i in range(6)]
    list_of_paid = [[] for i in range(2)]
    if start_date != '0' or end_date != '0':
        invoice_data = Invoice.objects.only("invoice_number", "created_date", "reviewed_date", "total_amount",
                                            "due_date", "voided_date", "project_details")\
            .select_related('project_details').filter(created_date__gt=start_date, created_date__lte=end_date)
        invoice_list = Invoice.objects.only("id", "created_date").filter(
            created_date__gt=start_date, created_date__lte=end_date).values_list('id')
        invoicepayment_data = InvoicePayment.objects.only("invoice", "amount").filter(
            invoice_id__in=invoice_list).select_related('invoice')
        for each in invoice_data:
            if each.due_date and (datetime.utcnow().date() - each.due_date).days > 0 and each.voided_date is None:
                list_of_data[0].append(str(each.invoice_number))
                list_of_data[1].append(str(utc_to_local(each.created_date))[0:19])
                list_of_data[2].append(str(each.due_date)[0:19])
                list_of_data[3].append(float(each.total_amount))
                list_of_data[4].append((datetime.utcnow().date() - each.due_date).days)
                list_of_data[5].append(str(each.project_details.project))

        for paid in invoicepayment_data:
            list_of_paid[0].append(str(paid.invoice.invoice_number))
            list_of_paid[1].append(float(paid.amount))
        print(list_of_data)
        list_totalamount = list(map(list, itertools.zip_longest(*list_of_data, fillvalue=None)))
        list_paid = list(map(list, itertools.zip_longest(*list_of_paid, fillvalue=None)))
        # print(list_totalamount)
        # print(list_paid)
        df1 = pd.DataFrame(list_totalamount, columns=['Invoice', 'Created', 'DueDate', 'totalamount', 'Overdue', 'Project'])
        df2 = pd.DataFrame(list_paid, columns=['Invoice', 'amount'])
        df2 = df2.groupby(['Invoice']).aggregate({'amount': 'sum'}).reset_index()
        left_join = pd.merge(df1, df2, on='Invoice', how='left')
        left_join['amount'] = left_join['amount'].fillna(0)
        left_join['Outstanding'] = left_join['totalamount'] - left_join['amount']
        joined = left_join.drop('totalamount', axis=1)
        joined_data = joined.drop('amount', axis=1)
        d = {'Outstanding': 'sum'}
        joined_data = joined_data.groupby(['Invoice', 'Created', 'DueDate', 'Overdue', 'Project']).aggregate(d).reset_index()
        joined_data['Outstanding'] = joined_data['Outstanding'].apply(lambda x: "${:,.2f}".format(x))
        joined_data = joined_data[joined_data.Outstanding != '$0.00']
        output = joined_data.to_dict('records')
        return render(request, "reports/aging_schedule.html",
                      {'context': output, 'start': start_date, 'end': end_date})
    else:
        return render(request, "reports/aging_schedule.html", {'start': start_date, 'end': end_date})
