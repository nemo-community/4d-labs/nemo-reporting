from setuptools import find_packages, setup

VERSION = "1.3.0"

setup(
    name="NEMO-reporting",
    version=VERSION,
    python_requires=">=3.7",
    description="Install Reporting plugin for NEMO",
    long_description="Reporting plugin for NEMO",
    author="4D LABS",
    author_email="chloe_zhu@sfu.ca",
    url="https://gitlab.com/nemo-community/4d-labs/nemo-reporting",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "django",
        "python-dateutil",
		"pandas==2.2.2"
    ],
    extras_require={
        "NEMO-CE": ["NEMO-CE>=6.0.0"],
        "NEMO": ["NEMO>=6.0.0"],
    },
    license="MIT",
    keywords=["NEMO"],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.7",
    ],
)
