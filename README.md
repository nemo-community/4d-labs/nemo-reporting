# NEMO reporting

## Installation

To install a specific version (1.0 for example), use:

`pip install git+https://gitlab.com/nemo-community/4d-labs/nemo-reporting.git@1.0`

(version numbers can be found [here](https://gitlab.com/nemo-community/4d-labs/nemo-reporting/-/tags/))

in `settings.py` add to `INSTALLED_APPS`:

```python
INSTALLED_APPS = [
    '...',
    'NEMO_reporting',
    '...'
]
```

## NEMO Compatibility

NEMO Reporting is compatible with NEMO >= 3.14.0 and NEMO-Billing >= 1.15.0 (optional)

## Usage

Once installed, simply navigate to `<nemo_address>/reports`

For easier access, consider adding a dashboard item (icon available for download [here](https://gitlab.com/nemo-community/4d-labs/nemo-reporting/-/raw/master/NEMO_reporting/report.png?inline=false)).
